import { Values } from "./types";
import { Wrapper } from "./styles";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Form from "../../features/form";
import FormProvider, { useFormData } from "./context";
import React, { useState } from "react";
import Stepper from "../../features/stepper";
import Summary from "../../features/summary";

const schemaDelivery = yup
  .object()
  .shape({
    email: yup
      .string()
      .required("Masukan Email Anda")
      .email("Email tidak valid"),
    address: yup
      .string()
      .required("Masukan Alamat Anda")
      .max(120, "Maksimal 120 digit"),
    dropshipperPhone: yup.string().when("isDropshipper", {
      is: true,
      then: yup
        .string()
        .required("Masukan Nomor Dropshipper")
        .matches(/^\d+$/, "Masukan hanya angka")
        .max(20, "Maksimal 20 digit")
        .min(3, "Minimal 3 digit"),
    }),
    dropshipper: yup.string().when("isDropshipper", {
      is: true,
      then: yup.string().required("Masukan Nama Dropshipper"),
    }),
    phone: yup
      .string()
      .required("Masukan Nomor Anda")
      .matches(/^\d+$/, "Masukan hanya angka")
      .max(20, "Maksimal 20 digit")
      .min(3, "Minimal 3 digit"),
  })
  .required();

const schemaShipment = yup
  .object()
  .shape({
    shipment: yup
      .string()
      .required("Pilih Pengantaran Anda")
      .typeError("Pilih Pengantaran Anda"),
    payment: yup
      .string()
      .required("Pilih Pembayaran Anda")
      .typeError("Pilih Pembayaran Anda"),
  })
  .required();

const CheckoutViews = () => {
  const step = localStorage.getItem("step") || 1;
  const formData = useFormData();
  const [formStep, setFormStep] = useState(Number(step));
  const goToStep = (step: number) => setFormStep(step);

  const {
    handleSubmit,
    register,
    getValues,
    formState: { errors },
  } = useForm<Values>({
    resolver: yupResolver(formStep === 1 ? schemaDelivery : schemaShipment),
    mode: "all",
  });

  const onSubmit = handleSubmit((data) => {
    goToStep(formStep + 1);
    formData?.setFormValues?.({
      ...data,
    });
    localStorage.setItem("formData", JSON.stringify(data));
    localStorage.setItem("step", JSON.stringify(formStep));
  });

  return (
    <FormProvider>
      <Wrapper>
        <Stepper formStep={formStep} goToStep={goToStep} />
        <form onSubmit={onSubmit}>
          <Form
            formStep={formStep}
            goToStep={goToStep}
            register={register}
            errors={errors}
            getValues={getValues}
          />
          <Summary formStep={formStep} getValues={getValues} />
        </form>
      </Wrapper>
    </FormProvider>
  );
};

export default CheckoutViews;
