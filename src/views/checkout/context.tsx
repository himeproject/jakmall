import React, { useState, createContext, useContext } from "react";
import { Values } from "./types";

type ChildrenType = {
  children: JSX.Element[] | JSX.Element;
};

const existingForm = localStorage.getItem("formData");

const defaultState = existingForm
  ? JSON.parse(existingForm)
  : {
      email: "",
      dropshipper: "",
      isDropshipper: false,
      phone: "",
      dropshipperPhone: "",
      address: "",
      shipment: "",
      payment: "",
    };

export const FormContext = createContext<Values | null>(null);

export default function FormProvider({ children }: ChildrenType) {
  const [data, setData] = useState(defaultState);

  const setFormValues = (values: Values) => {
    setData((prevValues: any) => ({
      ...prevValues,
      ...values,
    }));
  };

  return (
    <FormContext.Provider value={{ ...data, setFormValues }}>
      {children}
    </FormContext.Provider>
  );
}

export const useFormData = () => useContext(FormContext);
