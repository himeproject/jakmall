export type Values = {
  email: string;
  dropshipper: string;
  isDropshipper: boolean;
  phone: string;
  dropshipperPhone: string;
  address: string;
  shipment: string;
  payment: string;
  setFormValues?: (values: Values) => void;
};
