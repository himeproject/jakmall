import styled from "styled-components";

export const Wrapper = styled.div`
  position: relative;
  background: #ffffff;
  box-shadow: 2px 10px 20px rgba(255, 138, 0, 0.1);
  border-radius: 4px;
  margin: 55px 50px 95px 50px;
  height: 550px;

  > form {
    display: flex;
  }

  @media only screen and (max-width: 768px) {
    height: fit-content;
    margin: 24px;

    > form {
      flex-wrap: wrap;
    }
  }
`;
