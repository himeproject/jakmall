export type Steps = {
  formStep: number;
  goToStep?: (step: number) => void;
  register?: any;
  errors?: any;
  getValues?: any;
};
