export type OptionType = {
  name: string;
  title: string;
  price: number;
};
