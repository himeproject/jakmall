import CheckoutViews from "../views/checkout";
import React from "react";

const Checkout = () => {
  return <CheckoutViews />;
};

export default Checkout;
