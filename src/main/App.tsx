import Checkout from "../pages/checkout";
import React from "react";
import Theme from "./Theme";

function App() {
  return (
    <div>
      <Theme />
      <Checkout />
    </div>
  );
}

export default App;
