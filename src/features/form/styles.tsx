import styled from "styled-components";

export const Wrapper = styled.div`
  width: 75%;
  padding: 0 30px 0 40px;

  @media only screen and (max-width: 768px) {
    width: 100%;
    padding: 0 20px;
  }
`;

export const BackButton = styled.div`
  display: flex;
  align-items: center;
  margin: 30px 0 24px;
  cursor: pointer;

  > img {
    margin: 0 10px 0 0;
  }

  > p {
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
    color: #000000;
    mix-blend-mode: normal;
    opacity: 0.6;
  }

  @media only screen and (max-width: 768px) {
    flex-wrap: wrap;
  }
`;
