import { Steps } from "../../types/steps";
import { BackButton, Wrapper } from "./styles";
import CompletedPurchase from "./components/Completed";
import DeliveryForm from "./components/Delivery";
import React from "react";
import ShipmentForm from "./components/Shipment";
import arrowBack from "./assets/arrow_back.svg";

const Form = ({ formStep, goToStep, register, errors, getValues }: Steps) => {
  const renderForm = () => {
    switch (formStep) {
      case 1:
        return (
          <DeliveryForm
            formStep={formStep}
            goToStep={goToStep}
            register={register}
            errors={errors}
            getValues={getValues}
          />
        );
      case 2:
        return (
          <ShipmentForm
            formStep={formStep}
            goToStep={goToStep}
            register={register}
            errors={errors}
            getValues={getValues}
          />
        );
      case 3:
        return (
          <CompletedPurchase
            formStep={formStep}
            goToStep={goToStep}
            register={register}
            errors={errors}
          />
        );

      default:
        return (
          <DeliveryForm
            formStep={formStep}
            goToStep={goToStep}
            register={register}
            errors={errors}
          />
        );
    }
  };

  const stepBack = () => {
    if (formStep === 1) {
      goToStep?.(1);
    } else {
      goToStep?.(formStep - 1);
    }
  };

  return (
    <Wrapper>
      {formStep <= 2 && (
        <BackButton onClick={stepBack}>
          <img src={arrowBack} alt="Back Button" />
          <p>Back to {formStep === 2 ? "Delivery" : "Cart"}</p>
        </BackButton>
      )}
      {renderForm()}
    </Wrapper>
  );
};

export default Form;
