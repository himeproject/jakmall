import { Steps } from "../../../../types/steps";
import { Wrapper } from "./styles";
import { customAlphabet } from "nanoid";
import { shipmentStatus } from "../../../summary/components/BottomSummary/Shipment";
import { useFormData } from "../../../../views/checkout/context";
import React from "react";

const CompletedPurchase = ({ formStep, goToStep }: Steps) => {
  const nanoid = customAlphabet("ABCDEFGHJKLMNPQRSTUVWXYZ23456789", 5);
  const orderID = nanoid();
  const formData = useFormData();

  return (
    <Wrapper>
      <h1>Thank you</h1>
      <div>
        <p>Order ID : {orderID}</p>
        <p>
          Your order will be delivered{" "}
          {shipmentStatus(formData?.shipment || "").estimate} with{" "}
          {shipmentStatus(formData?.shipment || "").name}
        </p>
      </div>
      <button onClick={() => goToStep?.(1)}>Go To Homepage</button>
    </Wrapper>
  );
};

export default CompletedPurchase;
