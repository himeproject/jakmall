import styled from "styled-components";

export const Wrapper = styled.div`
  position: relative;
  left: 100%;
  top: 50%;
  transform: translate(-50%, -50%);

  > h1 {
    font-family: "Montserrat";
    font-style: normal;
    font-weight: 700;
    font-size: 36px;
    line-height: 44px;
    color: #ff8a00;
    margin: 0 0 26px;
  }

  > div > p:first-child,
  > div > p:last-child,
  > button {
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    color: #000000;
  }

  > div {
    > p:last-child {
      mix-blend-mode: normal;
      opacity: 0.6;
      margin: 9px 0 0;
    }
  }

  > button {
    border: none;
    background: none;
    cursor: pointer;
    opacity: 0.6;
    margin: 60px 0 0;
    padding: 0;
  }

  @media only screen and (max-width: 768px) {
    position: relative;
    left: 50%;
    top: 25%;
    transform: translate(-50%, 0);
  }
`;
