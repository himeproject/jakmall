import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  gap: 30px;

  > div {
    position: relative;

    > input,
    textarea {
      font-family: "Inter";
      height: 60px;
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      line-height: 19px;
      color: #000000;
      padding: 15px 20px;
    }

    > .small {
      width: 300px;
    }

    > .large {
      width: 400px;
    }

    > .address {
      width: 400px;
      height: 120px;
    }

    > .valid {
      border: 1px solid #1bd97b;
    }

    > .error {
      border: 1px solid #ff8a00;
    }

    > p,
    .count {
      position: absolute;
      color: #ff8a00;
      margin: 4px 0 0;
      font-style: italic;
      font-size: 12px;
    }

    > .count {
      right: 0;
      color: #000000;
    }
  }

  @media only screen and (max-width: 768px) {
    > div {
      width: 100%;
      
      > .small,
      > .large,
      > .address {
        width: 100%;
      }
    }
  }
`;

export const Title = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 0 0 36px;
  font-family: "Montserrat";
  font-style: normal;
  font-weight: 700;
  font-size: 24px;
  line-height: 40px;
  color: #ff8a00;

  > div {
    > label {
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      line-height: 17px;
      color: #2d2a40;
      mix-blend-mode: normal;
      opacity: 0.8;
      cursor: pointer;
    }
  }

  @media only screen and (max-width: 768px) {
    flex-wrap: wrap;
  }
`;
