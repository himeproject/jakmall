import { Steps } from "../../../../types/steps";
import { Wrapper, Title } from "./styles";
import React from "react";

const DeliveryForm = ({
  formStep,
  goToStep,
  register,
  errors,
  getValues,
}: Steps) => {
  const values = getValues();
  const [currentFocus, setCurrentFocus] = React.useState<string>("");
  const [count, setCount] = React.useState<number>(0);
  const [disabled, setDisabled] = React.useState<boolean>(values.isDropshipper);
  const handleChange = (e: any) => {
    setCurrentFocus(e.target.id);
    if (e.target.id === "address") {
      setCount(e.target.value.length);
    }
  };
  const handleCheckbox = (e: any) => {
    setDisabled(!disabled);
  };

  const isErrorOrValid = () => {
    const element = document.getElementById(currentFocus);
    errors[currentFocus]
      ? element?.classList.add("error")
      : element?.classList.add("valid");
  };

  return (
    <>
      <Title>
        <h1>Delivery details</h1>
        <div>
          <input
            type="checkbox"
            id="isDropshipper"
            {...register?.("isDropshipper")}
            className={`large ${isErrorOrValid()}`}
            onClick={(e) => handleCheckbox(e)}
          />
          <label htmlFor="isDropshipper"> Send as dropshipper</label>
        </div>
      </Title>
      <Wrapper>
        <div>
          <input
            type="email"
            id="email"
            placeholder="Email"
            {...register?.("email", { required: true })}
            className={`large ${isErrorOrValid()}`}
            onChange={(e) => handleChange(e)}
          />
          {errors?.email && <p>{errors.email.message}</p>}
        </div>
        <div>
          <input
            type="text"
            id="dropshipper"
            placeholder="Dropshipper Name"
            {...register?.("dropshipper", { required: true })}
            className={`large ${isErrorOrValid()}`}
            onChange={(e) => handleChange(e)}
            disabled={!disabled}
          />
          {errors?.dropshipper && <p>{errors.dropshipper.message}</p>}
        </div>
        <div>
          <input
            type="text"
            id="phone"
            placeholder="Phone Number"
            {...register?.("phone", { required: true })}
            className={`large ${isErrorOrValid()}`}
            onChange={(e) => handleChange(e)}
          />
          {errors?.phone && <p>{errors.phone.message}</p>}
        </div>
        <div>
          <input
            type="text"
            placeholder="Dropshipper phone number"
            id="dropshipperPhone"
            {...register?.("dropshipperPhone", { required: true })}
            className={`large ${isErrorOrValid()}`}
            onChange={(e) => handleChange(e)}
            disabled={!disabled}
          />
          {errors?.dropshipperPhone && <p>{errors.dropshipperPhone.message}</p>}
        </div>
        <div>
          <textarea
            type="text"
            id="address"
            placeholder="Delivery Address"
            {...register?.("address", { required: true })}
            className={`address ${isErrorOrValid()}`}
            onChange={(e) => handleChange(e)}
          />
          <p className="count">{count}/120 karakter</p>
          {errors?.address && <p>{errors.address.message}</p>}
        </div>
      </Wrapper>
    </>
  );
};

export default DeliveryForm;
