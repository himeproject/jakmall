import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  gap: 30px;
`;

export const Title = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 0 0 36px;
  font-family: "Montserrat";
  font-style: normal;
  font-weight: 700;
  font-size: 24px;
  line-height: 40px;
  color: #ff8a00;

  > div {
    > label {
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      line-height: 17px;
      color: #2d2a40;
      mix-blend-mode: normal;
      opacity: 0.8;
      cursor: pointer;
    }
  }
`;
