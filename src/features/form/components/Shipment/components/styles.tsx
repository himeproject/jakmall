import styled from "styled-components";

export const Wrapper = styled.div`
  > h1 {
    font-family: "Montserrat";
    font-style: normal;
    font-weight: 700;
    font-size: 36px;
    line-height: 44px;
    color: #ff8a00;
  }

  > div {
    margin: 30px 0 0;
    display: flex;
    gap: 10px;
  }

  > p {
    position: absolute;
    color: #ff8a00;
    margin: 4px 0 0;
    font-style: italic;
    font-size: 12px;
  }

  @media only screen and (max-width: 768px) {
    > div {
      flex-wrap: wrap;
    }
  }
`;
