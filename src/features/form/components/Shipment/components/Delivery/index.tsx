import { OptionType } from "../../../../../../types/shipment";
import { Steps } from "../../../../../../types/steps";
import { Wrapper } from "../styles";
import Options from "../Options";
import React from "react";

const DeliveryOptions = ({
  getValues,
  register,
  errors,
}: Omit<Steps, "formStep">) => {
  const deliveryOptions = [
    {
      name: "go-send",
      price: 15000,
      title: "GO-SEND",
    },
    {
      name: "jne",
      price: 9000,
      title: "JNE",
    },
    {
      name: "personal-courier",
      price: 29000,
      title: "Personal Courier",
    },
  ];

  return (
    <Wrapper>
      <h1>Shipment</h1>
      <div>
        {deliveryOptions.map(({ name, title, price }: OptionType, index) => (
          <Options
            name={name}
            title={title}
            price={price}
            key={index}
            register={register}
            getValues={getValues}
            errors={errors}
            id="shipment"
          />
        ))}
      </div>
      {errors?.shipment && <p>{errors.shipment.message}</p>}
    </Wrapper>
  );
};

export default DeliveryOptions;
