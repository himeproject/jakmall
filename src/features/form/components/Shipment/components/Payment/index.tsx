import { OptionType } from "../../../../../../types/shipment";
import { Steps } from "../../../../../../types/steps";
import { Wrapper } from "../styles";
import Options from "../Options";
import React from "react";

const PaymentOptions = ({
  getValues,
  register,
  errors,
}: Omit<Steps, "formStep">) => {
  const paymentOptions = [
    {
      name: "e-wallet",
      price: 1500000,
      title: "e-Wallet",
    },
    {
      name: "bank",
      title: "Bank Transfer",
      price: 0,
    },
    {
      name: "virtual-account",
      title: "Virtual Account",
      price: 0,
    },
  ];

  return (
    <Wrapper>
      <h1>Payment</h1>
      <div>
        {paymentOptions.map(({ name, title, price }: OptionType, index) => (
          <Options
            name={name}
            title={title}
            price={price}
            key={index}
            register={register}
            getValues={getValues}
            errors={errors}
            id="payment"
          />
        ))}
      </div>
      {errors?.payment && <p>{errors.payment.message}</p>}
    </Wrapper>
  );
};

export default PaymentOptions;
