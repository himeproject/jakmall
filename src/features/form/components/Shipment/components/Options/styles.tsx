import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  gap: 10px;

  > label {
    > input[type="radio"] {
      display: none;
    }

    .radio-input + .card {
      background: #ffffff;
      border: 1px solid #cccccc;
      width: 180px;
      height: 60px;
      padding: 12px 15px;

      > .title {
        font-weight: 500;
        font-size: 13px;
        line-height: 16px;
        color: #000000;
        mix-blend-mode: normal;
        opacity: 0.8;
      }

      > .price {
        font-weight: 700;
        font-size: 16px;
        line-height: 19px;
        color: #2d2a40;
      }
    }

    .radio-input:checked + .card {
      background: rgba(27, 217, 123, 0.1);
      border: 2px solid #1bd97b;
    }

    > .card {
      display: flex;
      flex-direction: column;
      justify-content: center;
    }
  }

  > :hover {
    cursor: pointer;
  }

  @media only screen and (max-width: 768px) {
    width: 100%;

    > label {
      width: 100%;
      .radio-input + .card {
        width: 100%;
      }
    }
  }
`;
