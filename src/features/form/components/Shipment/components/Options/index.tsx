import { OptionType } from "../../../../../../types/shipment";
import { Steps } from "../../../../../../types/steps";
import { Wrapper } from "./styles";
import { useFormData } from "../../../../../../views/checkout/context";
import React from "react";

type Id = {
  id: string;
};

type OptionWithSteps = OptionType & Omit<Steps, "formStep"> & Id;

const Options = ({
  name,
  price,
  title,
  register,
  id,
  getValues,
}: OptionWithSteps) => {
  const values = getValues();
  const formData = useFormData();
  const handleClick = () => {
    formData?.setFormValues?.({ ...values, [id]: name });
  };

  return (
    <Wrapper>
      <label>
        <input
          type="radio"
          value={name}
          name={id}
          id={`${id}-${name}`}
          {...register?.(id)}
          className="radio-input"
          onClick={() => handleClick()}
        />
        <div className="card">
          <div className="title">{title}</div>
          {price > 0 && (
            <div className="price">
              {price} {name === "e-wallet" && "left"}
            </div>
          )}
        </div>
      </label>
    </Wrapper>
  );
};

export default Options;
