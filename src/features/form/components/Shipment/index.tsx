import { Steps } from "../../../../types/steps";
import { Wrapper } from "./styles";
import DeliveryOptions from "./components/Delivery";
import PaymentOptions from "./components/Payment";
import React from "react";

const ShipmentForm = ({
  formStep,
  goToStep,
  getValues,
  register,
  errors,
}: Steps) => {
  return (
    <Wrapper>
      <DeliveryOptions
        getValues={getValues}
        register={register}
        errors={errors}
      />
      <PaymentOptions
        getValues={getValues}
        register={register}
        errors={errors}
      />
    </Wrapper>
  );
};

export default ShipmentForm;
