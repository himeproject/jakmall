import styled from "styled-components";

export const Wrapper = styled.button`
  background: #ff8a00;
  border: 1px solid rgba(255, 255, 255, 0.2);
  box-shadow: 3px 5px 10px rgba(255, 138, 0, 0.2);
  border-radius: 2px;
  width: 100%;
  margin: 30px 0 0;
  padding: 20px 40px;
  color: #ffffff;

  > input[type="submit"] {
    cursor: pointer;
    background: none;
    border: none;
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 22px;
    text-align: center;
    color: #ffffff;
  }
`;
