import { Steps } from "../../../../types/steps";
import { Wrapper } from "./styles";
import React from "react";
import { useFormData } from "../../../../views/checkout/context";

export const choosedPaymentMethod = (payment: string) => {
  const result = {
    name: "",
  };

  switch (payment) {
    case "bank":
      result.name = "Bank Transfer";
      break;
    case "virtual-account":
      result.name = "Virtual Account";
      break;
    case "e-wallet":
      result.name = "e-Wallet";
      break;
    default:
      result.name = "Pilih Pembayaran";
      break;
  }

  return result;
};

const Button = ({ formStep }: Steps) => {
  const formData = useFormData();

  const renderText = () => {
    if (formStep === 1) {
      return <input type="submit" value="Continue to Payment" />;
    }

    return (
      <input
        type="submit"
        value={
          formData?.payment === ""
            ? "Pilih Pembarayan"
            : `Pay with ${choosedPaymentMethod(formData?.payment || "").name}`
        }
      />
    );
  };
  if (formStep > 2) return null;

  return <Wrapper>{renderText()}</Wrapper>;
};

export default Button;
