import { Subtotal } from "./styles";
import { useFormData } from "../../../../views/checkout/context";
import React from "react";

export const shipmentStatus = (shipment: string) => {
  let result = {
    name: "",
    price: 0,
    estimate: "",
  };

  switch (shipment) {
    case "jne":
      result.name = "JNE";
      result.price = 9000;
      result.estimate = "2 days";
      break;
    case "personal-courier":
      result.name = "Personal Courier";
      result.price = 29000;
      result.estimate = "1 day";
      break;
    case "go-send":
      result.name = "GO-SEND";
      result.price = 15000;
      result.estimate = "today";
      break;

    default:
      break;
  }

  return result;
};

export default function Shipment() {
  const formData = useFormData();

  if (
    formData?.shipment === "" ||
    shipmentStatus(formData?.shipment || "").name === ""
  )
    return null;
  return (
    <Subtotal>
      <p>
        <span>{shipmentStatus(formData?.shipment || "").name}</span> shipment
      </p>
      <p>{shipmentStatus(formData?.shipment || "").price}</p>
    </Subtotal>
  );
}
