import { Subtotal } from "./styles";
import React from "react";

export default function Fee() {
  return (
    <Subtotal>
      <p>Cost of goods</p>
      <p>500000</p>
    </Subtotal>
  );
}
