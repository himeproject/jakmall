import styled from "styled-components";

export const Wrapper = styled.div`
  .title {
    font-family: "Montserrat";
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    line-height: 29px;
    color: #ff8a00;
    margin: 0 0 10px;
  }

  .purchased-items {
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    color: #000000;
    mix-blend-mode: normal;
    opacity: 0.6;
  }
`;

export const Subtotal = styled.div`
  margin: 0 0 12px;
  display: flex;
  justify-content: space-between;

  > p {
    font-style: normal;
    font-size: 14px;
    line-height: 17px;
    color: #000000;

    > span {
      font-weight: 700;
    }
  }
  > p:first-child {
    font-weight: 400;
    mix-blend-mode: normal;
    opacity: 0.6;
  }

  > p:last-child {
    font-weight: 700;
  }
`;

export const TotalText = styled.div`
  display: flex;
  justify-content: space-between;
  font-family: "Montserrat";
  font-style: normal;
  font-weight: 700;
  font-size: 24px;
  line-height: 29px;
  color: #ff8a00;
  margin: 24px 0 0;
`;
