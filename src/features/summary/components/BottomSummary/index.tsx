import { Steps } from "../../../../types/steps";
import { Wrapper } from "./styles";
import Dropshipper from "./Dropshipper";
import Fee from "./Fee";
import React from "react";
import Shipment from "./Shipment";
import Total from "./Total";
import Button from "../Button";

const BottomSummary = ({ formStep, getValues }: Steps) => {
  return (
    <Wrapper>
      <Fee />
      <Dropshipper formStep={formStep} getValues={getValues} />
      <Shipment />
      <Total />
      <Button formStep={formStep} />
    </Wrapper>
  );
};

export default BottomSummary;
