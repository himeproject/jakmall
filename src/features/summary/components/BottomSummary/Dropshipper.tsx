import { Steps } from "../../../../types/steps";
import { Subtotal } from "./styles";
import React from "react";

const Dropshipper = ({ formStep, getValues }: Steps) => {
  const { isDropshipper } = getValues();

  if (isDropshipper && formStep > 1) {
    return (
      <Subtotal>
        <p>Dropshipping Fee</p>
        <p>5900</p>
      </Subtotal>
    );
  }

  return null;
};

export default Dropshipper;
