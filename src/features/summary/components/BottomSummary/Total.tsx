import { TotalText } from "./styles";
import React from "react";
import { useFormData } from "../../../../views/checkout/context";
import { shipmentStatus } from "./Shipment";

export default function Total() {
  const formData = useFormData();
  const shipmentPrice = shipmentStatus(formData?.shipment || "").price;
  const dropshipperFee = formData?.isDropshipper ? 5900 : 0;
  const total = 5000000 + shipmentPrice + dropshipperFee;
  
  return (
    <TotalText>
      <p>Total</p>
      <p>{total}</p>
    </TotalText>
  );
}
