import { Steps } from "../../../../types/steps";
import { Text } from "./styles";
import { choosedPaymentMethod } from "../Button";
import { useFormData } from "../../../../views/checkout/context";
import React from "react";

const Delivery = ({ formStep }: Pick<Steps, "formStep">) => {
  const formData = useFormData();
  if (formStep < 3) return null;

  return (
    <Text>
      <hr />
      <p>Payment method</p>
      <p>{choosedPaymentMethod(formData?.payment || "").name}</p>
    </Text>
  );
};

export default Delivery;
