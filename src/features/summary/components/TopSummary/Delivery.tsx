import { Steps } from "../../../../types/steps";
import { Text } from "./styles";
import { shipmentStatus } from "../BottomSummary/Shipment";
import { useFormData } from "../../../../views/checkout/context";
import React from "react";

const Delivery = ({ formStep }: Pick<Steps, "formStep">) => {
  const formData = useFormData();
  const shipment = shipmentStatus(formData?.shipment || "");
  if (formStep < 2 || shipment.name === "") return null;

  return (
    <Text>
      <hr />
      <p>Delivery estimation</p>
      <p>
        {shipment.estimate} by {shipment.name}
      </p>
    </Text>
  );
};

export default Delivery;
