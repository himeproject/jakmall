import styled from "styled-components";

export const Wrapper = styled.div`
  .title {
    font-family: "Montserrat";
    font-style: normal;
    font-weight: 700;
    font-size: 24px;
    line-height: 29px;
    color: #ff8a00;
    margin: 0 0 10px;
  }

  .purchased-items {
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    color: #000000;
    mix-blend-mode: normal;
    opacity: 0.6;
  }
`;

export const Text = styled.div`
  > hr {
    margin: 21px 0;
    width: 80px;
    border: none;
    border-top: 1px solid #d8d8d8;
  }

  > p:nth-child(2) {
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    color: #000000;
    margin: 0 0 4px;
  }

  > p:last-child {
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    color: #1bd97b;
  }
`;
