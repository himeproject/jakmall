import { Steps } from "../../../../types/steps";
import { Wrapper } from "./styles";
import Delivery from "./Delivery";
import Payment from "./Payment";
import React from "react";

const TopSummary = ({ formStep }: Pick<Steps, "formStep">) => {
  return (
    <Wrapper>
      <p className="title">Summary</p>
      <p className="purchased-items">10 items purchased</p>
      <Delivery formStep={formStep} />
      <Payment formStep={formStep} />
    </Wrapper>
  );
};

export default TopSummary;
