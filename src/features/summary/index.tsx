import { Steps } from "../../types/steps";
import { Wrapper } from "./styles";
import BottomSummary from "./components/BottomSummary";
import React from "react";
import TopSummary from "./components/TopSummary";

const Summary = ({ formStep, getValues }: Steps) => {
  return (
    <Wrapper>
      <TopSummary formStep={formStep} />
      <BottomSummary formStep={formStep} getValues={getValues} />
    </Wrapper>
  );
};

export default Summary;
