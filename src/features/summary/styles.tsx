import styled from "styled-components";

export const Wrapper = styled.div`
  margin: 50px 0 20px;
  width: 25%;
  padding: 20px;
  border-left: solid 1px rgba(255, 138, 0, 0.2);
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media only screen and (max-width: 768px) {
    width: 100%;
  }
`;
