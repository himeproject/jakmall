import { Wrapper } from "./styles";
import React from "react";
import { Steps } from "../../types/steps";
import arrowRight from "./assets/arrow_right.svg";

const Stepper = ({ formStep }: Steps) => {
  const steps = [
    {
      id: 1,
      name: "Delivery",
    },
    {
      id: 2,
      name: "Payment",
    },
    {
      id: 3,
      name: "Finish",
    },
  ];

  return (
    <Wrapper>
      <ul>
        {steps.map((step, index) => (
          <li key={index}>
            <p className={step.id <= formStep ? "active" : ""}>{step.id}</p>
            {step.name}
            {step.id !== 3 && <img src={arrowRight} alt="Arrow Right" />}
          </li>
        ))}
      </ul>
    </Wrapper>
  );
};

export default Stepper;
