import styled from "styled-components";

export const Wrapper = styled.div`
  background: #fffae6;
  border-radius: 35px;
  position: absolute;
  width: 500px;
  height: 70px;
  top: -35px;
  left: 50%;
  transform: translate(-50%, 0);

  > ul {
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 0 21px;
    padding: 0;

    > li {
      list-style-type: none;
      display: flex;
      align-items: center;
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      line-height: 19px;
      color: #ff8a00;

      > p {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 30px;
        height: 30px;
        box-shadow: 0px 2px 4px rgba(255, 138, 0, 0.3);
        border-radius: 15px;
        margin: 0 10px 0 0;
        background: #fee5b8;
        color: #ff8a00;
      }

      > .active {
        background: #ff8a00;
        color: #ffffff;
      }

      > img {
        margin: 0 0 0 22px;
      }
    }
  }

  @media only screen and (max-width: 768px) {
    position: relative;
    top: 0
  }
`;
