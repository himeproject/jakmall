# Getting Started

## How to Install

- First clone my application with this syntax `https://gitlab.com/himeproject/jakmall.git`, make sure you have git first.

- Then go to application then run `yarn install` to install the application on your local, make sure you have node on your local and application run well on your local.

- Open browser with this link `http://localhost:3000/` to open on your local browser

- Finally the application run well

